
set fit errorvariables

set title "Heat Current as a funtion of Time"
set xlabel "Time [femtoseconds]
set ylabel "Heat Current [J K_{B}^{-1} fs^{-1}]"

KB=1.38065e-23
NA=6.022141e23
R=KB*NA
FIT_LIMIT=1e-5
ONSET=4000
TAU=1e3
ISS0=10

#fit [ONSET:*][*:*] ISS0*exp(-x/TAU)+ISS1*(1-exp(-x/TAU)) "gp.xvg" u 1:2 via ISS1,ISS0,TAU
fit [ONSET:*][*:*] ISS1 "gp.xvg" u 1:2 via ISS1

set term svg enhanced
set output "gp.svg"
set label "Steady state heat current:\n%.1f",ISS1*R," +/- %.1f",ISS1_err*R," KJ/mole ps^{-1}\nfor steady state onset at %.0f",ONSET," fs" at graph 0.2,0.8 left


#plot "gp.xvg" u  1:2 w d t "", ISS0*exp(-x/TAU)+ISS1*(1-exp(-x/TAU)) w lines t "fit"
plot "gp.xvg" u  1:2 w d t "", ISS1 w lines t "fit"

#set print "gp.dat" append
set print "gp.dat"
print ISS1*R,ISS1_err*R
