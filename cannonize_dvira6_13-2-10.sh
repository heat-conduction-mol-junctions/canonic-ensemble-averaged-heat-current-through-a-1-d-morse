#! /bin/bash
# this script runs 'dvira10.out' "MaxTraj" many times, each time with separate initial conditions randomally generated from a Maxwell-Boltzmann distribution at temperature "Tsys". It calls the C program 'ensemble.out' to join the output from previous runs ('TempEFile', etc.) with that of the current run ('EFile', etc.), giving each of the two the appropriate weight, and outputs into a third file ('E', etc.)

clear
#echo
#echo
#echo "		Cannonic Ensemble Averaged Heat Current Through a 1-D Morse Potential Chain"
#echo "		==========================================================================="
#echo
#echo

#echo
#echo "what is the number of trajectories to average?"
#read MaxTraj
#MaxTraj=$1
#N=$2

DATEANDTIME=$(date +%s%N)
INSTANCE=Morse$(echo ""$DATEANDTIME" 1000 / 1000000000000000 % 16 o p" | dc)
export INSTANCE
#echo $INSTANCE

read N Tsys L Ttot Dt SegLength Tl Gl Tr Gr MaxTraj < 13-2-10.par
#cp 13-2-10.par $INSTANCE.par
echo $N $Tsys $L $Ttot $Dt $SegLength $Tl $Gl $Tr $Gr $MaxTraj > $INSTANCE.par
PARAMETERFILE="$INSTANCE.par"
echo -e "variable        N       Tsys    l               ttot            dt      seglength T[0]     g[0]    T[1]    g[1]    MaxTraj\n\ntype            %d      %f      %f              %f              %f      %d        %f       %f      %f      %f\n\nunit            Angstrom K      Angstrom        fsec            fsec    time-stepsPHz      K       PHz\n\nmin.rec.value   0       0       (N-1)*xeq       0               0       1         0                                +2*xeq_0\n\nmax.rec.value   inf     inf     inf             inf             30      ttot/dt   Tmelt    1/100dt Tmelt   1/100dt" >> $INSTANCE.par

#echo -e "#INSTANCE N Tsys L Ttot Dt SegLength Tl Gl Tr Gr MaxTraj ISS ISSerr\n" >>currents.dat

# initiallize file name variables
E=E$MaxTraj.dat
Phase=Phase$MaxTraj.dat
J=J$MaxTraj.dat
Transf=Transf$MaxTraj.dat
Cont=Cont$MaxTraj.dat

traj=1; # initiallize trajectory counter variable

#echo ; echo
if [ -f J$MaxTraj.xvg ]
then
echo $MaxTraj already exists! please choose a different value...
echo ; echo
exit 1000
fi


# initiallize file name variables
E=E$INSTANCE.dat
Phase=Phase$INSTANCE.dat
J=J$INSTANCE.dat
Transf=Transf$INSTANCE.dat
Cont=Cont$INSTANCE.dat



#echo "deleting previous Temp* and output files"
#rm $E
#rm $Phase
#rm $J
#rm $Transf
#rm $Cont
#rm TempFile$E
#rm TempFile$Phase
#rm TempFile$J
#rm TempFile$Transf
#rm TempFile$Cont


# create 5 blank temporary files for the first trajectory to get (i.e., the first time 'ensemble.out' is run it will average with empty data in the blank files, and write new output files).
#echo
#echo "creating blank temporary files"
touch TempFile$E
touch TempFile$Phase
touch TempFile$J
touch TempFile$Transf
touch TempFile$Cont


while [ "$traj" -le "$MaxTraj" ]
do

#echo
#echo "-----------------------------------------------------------------------------------------------------------------"
#echo
#echo "		running trajectory nubmer $traj"

RandomizerSeed=$[$traj*1995]; # set Randomizer Seed for current trajectory, using 1995 as a constant seed of seeds

./dvira13-2-10.out "$traj" "$RandomizerSeed" "$PARAMETERFILE" "File$E" "File$Phase" "File$J" "File$Transf" "File$Cont"
# dvira10.out outputs 'FileE', etc.

# /////////////////////////////////////////////////////////////////////////////////////////////////////////////

# appearantly, dvira10.out gives some floating point error in line 114 of its code and ends with error code != 0

# suspect command-line arguments - file-names especially

# /////////////////////////////////////////////////////////////////////////////////////////////////////////////

# in case dvira10.out returns a bad exit code -- crash
if [ $? -ne 0 ]; then
echo "error running 'dvira10.out'. aborting."
exit 222
echo
fi



# ensemble.out reads 'FileE' & 'TempFileE', and outputs 'E', etc.
./cannonize10-2-10.out $traj $RandomizerSeed File$E File$Phase File$J File$Transf File$Cont TempFile$E TempFile$Phase TempFile$J TempFile$Transf TempFile$Cont $E $Phase $J $Transf $Cont $N

# in case cannonize10.out returns a bad exit code -- crash
if [ $? -ne 0 ]; then
echo "error running 'cannonize10.out'. aborting."
exit 222
echo
fi


# move updated
mv $E TempFile$E
mv $Phase TempFile$Phase
mv $J TempFile$J
mv $Transf TempFile$Transf
mv $Cont TempFile$Cont

traj=$[$traj+1]; #advance trajectory counter
done

#echo; echo
#echo "finished running last trajectory. now cleaning up"
#echo; echo

# move back to final positions
mv TempFile$E $E
mv TempFile$Phase $Phase
mv TempFile$J $J
mv TempFile$Transf  $Transf
mv TempFile$Cont  $Cont

# remove last-trajectory files
rm File$E
rm File$Phase
rm File$J
rm File$Transf
rm File$Cont


#xmgrace formatting information:

sed '1i\# this line is a comment line\
\@    page   size 842, 595\
\@    title \"Total System Energy\"\
\@    xaxis  label \"Time [femtoseconds]\"\
\@    yaxis  label \"Temperature \[\\1k\\0\\sB\\N\]\"\
\@TYPE xy\
\@ view 0.15, 0.15, 1.35, 0.875\
\@ legend on\
\@ legend loctype view\
\@ legend 1, 0.85\
\@ s0 legend \"Total System Energy\"
' $E > E$INSTANCE.xvg


sed '1i\# this line is a comment line\
\@    page   size 842, 595\
\@    title \"Nuclei Positions\"\
\@    xaxis  label \"Time [femtoseconds]\"\
\@    yaxis  label \"Position [Angstrom]\"\
\@TYPE xy\
\@ view 0.15, 0.15, 1.35, 0.875\
\@ legend on\
\@ legend loctype view\
\@ legend 1, 0.85\
\@ s0 legend \"nucleus 1\"\
\@ s1 legend \"nucleus 2\"\
\@ s2 legend \"nucleus 3\"
' $Phase > Phase$INSTANCE.xvg

sed '1i\# this line is a comment line\
\@    page   size 842, 595\
\@    title \"Heat Current Entering Nuclei From the Left\"\
\@    xaxis  label \"Time [femtoseconds]\"\
\@    yaxis  label \"Temperature [\\1k\\0\\sB\\N]\"\
\@TYPE xy\
\@ view 0.15, 0.15, 1.35, 0.875\
\@ legend on\
\@ legend loctype view\
\@ legend 0.85, 0.85\
\@ s0 legend \"nucleus 1\"\
\@ s1 legend \"nucleus 2\"\
\@ s2 legend \"nucleus 3\"\
\@ s3 legend \"leaving nucleus 3 to the right\"\
\@ s4 legend \"nucleus 1 & wall\"\
\@ s5 legend \"nucleus N & wall\"
' $J > J$INSTANCE.xvg

sed '1i\# this line is a comment line\
\@    page   size 842, 595\
\@    title \"Total Heat Transfered From the Left of each Nucleus \"\
\@    xaxis  label \"Time [femtoseconds]\"\
\@    yaxis  label \"Temperature [\\1k\\0\\sB\\N]\"\
\@TYPE xy\
\@ view 0.15, 0.15, 1.35, 0.875\
\@ legend on\
\@ legend loctype view\
\@ legend 0.85, 0.85\
\@ s0 legend \"nucleus 1\"\
\@ s1 legend \"nucleus 2\"\
\@ s2 legend \"nucleus 3\"\
\@ s3 legend \"leaving nucleus 3 to the right\"\
\@ s4 legend \"nucleus 1 & wall\"\
\@ s5 legend \"nucleus N & wall\"
' $Transf > Transf$INSTANCE.xvg

sed '1i\# this line is a comment line\
\@    page   size 842, 595\
\@    title \"Heat Continuity\"\
\@    subtitle \"Heat Lost Minus Heat Current Transfered\"\
\@    xaxis  label \"Time [femtoseconds]\"\
\@    yaxis  label \"Temperature [\\1k\\0\\sB\\N]\"\
\@TYPE xy\
\@ view 0.15, 0.15, 1.35, 0.875\
\@ legend on\
\@ legend loctype view\
\@ legend 1, 0.85\
\@ s0 legend \"nucleus 0\"\
\@ s1 legend \"nucleus 1\"\
\@ s2 legend \"nucleus 2\"\
\@ s3 legend \"nucleus 3\"\
\@ s4 legend \"nucleus 1 & wall\"\
\@ s5 legend \"nucleus N & wall\"
' $Cont > Cont$INSTANCE.xvg


rm $E
rm $Phase
rm $J
rm $Transf
rm $Cont


#xmgrace -nxy E$MaxTraj.xvg &
cp J$INSTANCE.xvg gp.xvg
#echo $MaxTraj >> currents.dat
gnuplot 10-2-10.gp
read ISS ISSerr < gp.dat
echo $INSTANCE $N $Tsys $L $Ttot $Dt $SegLength $Tl $Gl $Tr $Gr $MaxTraj $ISS $ISSerr >>currents.dat
mv gp.svg J$INSTANCE.svg
#eog J$INSTANCE.svg &


# exit sending a succesful flag to bash (exit code 0)
exit 0
