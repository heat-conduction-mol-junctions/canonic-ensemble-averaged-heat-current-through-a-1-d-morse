#! /bin/bash

DATEANDTIME=$(date +%s%N)
INSTANCE=$(echo ""$DATEANDTIME" 1000 / 1000000000000000 % 16 o p" | dc)
export INSTANCE


cp backup-13-2-10.par 13-2-10.par
read N Tsys L Ttot Dt SegLength Tl Gl Tr Gr MaxTraj < 13-2-10.par

DtempAvg=50
DtempD=50
iMax=1
jMax=1
i=0
j=0
while [ "$i" -le "$iMax" ]
do
	while [ "$j" -le "$jMax" ]
	do
		Tl=$[$Tl+$i*$DtempAvg+$j*$DtempD]
		Tr=$[$Tr+$i*$DtempAvg-$j*$DtempD]

		echo $N $Tsys $L $Ttot $Dt $SegLength $Tl $Gl $Tr $Gr $MaxTraj > 13-2-10.par
		echo $N $Tsys $L $Ttot $Dt $SegLength $Tl $Gl $Tr $Gr $MaxTraj
		qsub cannonize_dvira6_13-2-10b.sh -N Morse-$INSTANCE-$i-$j -d $PWD

		echo $i $j

	j=$[$j+1]
	done
i=$[$i+1]
done
