#include <stdlib.h>
#include <stdio.h>
#include <math.h>

//deffine maximum function
#define max( a, b ) ( ((a) > (b)) ? (a) : (b) )


/*simulation units
	[x]	=	Angstrom 				= 1e-10 meter
	[t]	=	femtosecond 				= 1e-15 second
	[v]	=	Angstrom/femtosecond 			= 1e5 meter/second
	[a]	=	Angstrom/femtosecond^2 			= 1e20 meter/second^2

	[E]	=	kcal/mole 				= 6.948e-21 J		~ 23.06 eV
	[F]	=	(Angstrom^-1)*(kcal/mole) 		= 6.948e-11 N
	[m]	=	(kcal/mole)*2*(Angstrom/femtosecond)^-2	= 1.3896e-30 kg		~ 1/1195 u (Unified atomic mass unit)

*/
//=====================================================================================================================================================

//variables

unsigned	N; 		//no. of nuclei in chain
double 		T[3]; 		//left (0) and right (1) bath temperatures and system initial temperature (2) in Kelvin
double		g[2]; 		//bath dissipation / coupling coefficients (gammas) in peta-Hertz (10^15 sec^-1)
double		R[2];		//random force on the two extreme nuclei, R[0] is the random force on the left-most, and R[1] on the right
double		ttot; 		//total simulated time in femtoseconds
unsigned	seglength;	//time length of total-energy-averaging time-segment in time-steps
double		dt;		//time step length in femtoseconds
double		maxstep;	//total no. of time-steps
unsigned	segs;		//total no. of time-segments ttot is divided into
double		l;		//total displacement between left and right adsorption sites in Angstrom
double		Eacc=0;		//total-energy accumulator for current time-segment in kcal/mole

//.....................................................................................................................................................

//constants

double		m=12;		//nucleus mass in u
const double	D=88;		//Morse model Dissociation energy in kcal/mole
const double	alpha=1.876;	//Morse model curvature at minimum energy in Angstrom^-1
const double	xeq=1.538;	//Morse model internuclear distance at minimum energy in Angstrom
double		kb=0.001986;	//Boltzmann constant in kcal/mole per Kelvin

double		vmelt;		//melting velocity (using Lindemann's criterion to find the kinetic energy needed to get a particle's vibrational amplitude to 0.1-(0.3)-0.4 times xeq

double		q,r,s;		//doubles for the random force algorithm


unsigned	i;		//nucleus index
unsigned	j;		//RK4 virtual-step index
unsigned	k;		//left (0) or right (0) bath index
unsigned	n;		//time-step index
unsigned	h=0;		//time-segment index

// Expanding the Morse potential to 2nd order about equilibrium (taking the harmonic approximation) leads to the small-amplitude vibration frequency omega = sqrt(2D*alpha^2 / m) ~ 3.5 attoseconds

//=====================================================================================================================================================

int main(int argc, char *argv[]){

	int	traj=atoi(argv[1]);//current trajectory (input by user at command line)
	int	RandSeed=atoi(argv[2]);//randomization seed (input by user at command line)

	FILE	*infile=fopen(argv[3],"rt");		//input parameters file
		if (infile==NULL){fputs ("\n\nError opening infile\n",stderr); exit (2);}
	FILE	*outfile=fopen(argv[4],"wt");		//total energy graph
		if (outfile==NULL){fputs ("\n\nError opening outfile\n",stderr); exit (2);}
	FILE	*PhaseFile=fopen(argv[5],"wt");	//phase graph
		if (PhaseFile==NULL){fputs ("\n\nError opening PhaseFile\n",stderr); exit (2);}
	FILE	*HeatCurrent=fopen(argv[6],"wt");	//heat current graph
		if (HeatCurrent==NULL){fputs ("\n\nError opening HeatCurrent\n",stderr); exit (2);}
	FILE	*HeatTransfered=fopen(argv[7],"wt");//transfered heat graph
		if (HeatTransfered==NULL){fputs ("\n\nError opening HeatTransfered\n",stderr); exit (2);}
	FILE	*HeatContinuity=fopen(argv[8],"wt");//heat continuity graph
		if (HeatContinuity==NULL){fputs ("\n\nError opening HeatContinuity\n",stderr); exit (2);}
        


//=====================================================================================================================================================

	fscanf(infile,"%d %lf %lf %lf %lf %d %lf %lf %lf %lf",&N,&T[2],&l,&ttot,&dt,&seglength,&T[0],&g[0],&T[1],&g[1]);
	fclose(infile);

	printf("\n");

	//echo input parameters
/*
	printf("\n\nInput Parameters:\n");
	printf("=================\n\n\n");
	printf("N =		%d\n",N);
	printf("Tsys (t=0) =	%6.6f\n",T[2]);
	printf("l =		%6.6f Angstroms\n",l);
	printf("ttot =		%6.6f femtoseconds\n",ttot);
	printf("dt =		%6.6f femtoseconds\n",dt);
	printf("seglength*dt =	%6.6f femtoseconds\n\n",seglength*dt);
	printf("Tl =		%6.6f Kelvin\n",T[0]);
	printf("gl =		%6.6f peta-Hz\n",g[0]);
	printf("Tr =		%6.6f Kelvin\n",T[1]);
	printf("gr =		%6.6f peta-Hz\n\n",g[1]);
*/

//.....................................................................................................................................................

	maxstep=ceil(ttot/dt);
//	printf("maxstep =	%g steps\n",maxstep);

	segs=ceil(maxstep/seglength);
//	printf("segs =		%d segments\n\n",segs);

	if ( segs > ceil(ttot/dt) ){
		printf("\n\nError! More time-segments than time-steps!\n");
		printf("-----------------------------------------------\n\n");
		}



	if ( ( (((int)(ceil(maxstep))) % segs) != 0 ) && ( ((int)(ceil(maxstep-1)) % segs) != 0 ) ){
//		printf("\n\nBeware! Non-constant number of time steps in each segment ( %d %d )!\n",(int) ((ceil(maxstep)) % segs),(int) ((ceil(maxstep-1)) % segs);
		printf("\n\nBeware! Non-constant number of time steps in each segment!\n");
		printf("-------------------------------------------------------------\n\n");
		}


//.....................................................................................................................................................

//=====================================================================================================================================================

//double	Eavg[segs];	//average total-energy in each time-segment -- Eavg[h] is the average total-energy in time-segment no. h

//all of the following tensors are deffined with certain lengths in terms of N+?. The index of the tensor element corresponds to the nucleus of the same index

double	phase[2][N+2];	//Cartesian/axial nuclear phase data structure -- phase[0][i] is the position of nucleus no. i, phase[1][i] is its velocity
			//nucleus 0 represents the location of the left adsorption site, and N+1 the right
double	e[N+1];		//Morse model exponential part of the potential, for each pair of nuclei, including 0 and N+1 (N+1 pairs in all)

//the following three tensors are deffined with some dimension of size N+1 -- this is done so that the i-th element corresponds to the i-th nucleus, while the zeroth element is left unused.
double	F[N+1];		//force on each nucleus in kcal/mole per Angstrom
double	V[2][N+1][4];	//RK4 slopes' tensor -- V[0][i][j] is the j-th step RK4 adjusted velocity of nucleus no. i, V[1][i][j] is its acceleration
double	Q[2][N+1][4];	//RK4 adjustment factors' tensor -- Q[0][i][j] is the j-th step RK4 adjustment of the velocity of nucleus no. i, V[1][i][j] is its acceleration's adjustment

double	FJ[N+1];	//pair-wise force between nucleus i and nucleus i+1 for local heat current calculation
double	Jacc[N+1];	//heat current accumulator for current time-segment in kcal/(mole*femtosecond)
double	FJ0=0,FJN=0,Jacc0=0,JaccN=0,EJ0=0,EJN=0;
double	EJ[N+1];	//total energy transfered to the right of nucleus i

//=====================================================================================================================================================


//convert units of mass from u to kcal/mole per Angstrom/femtosecond squared

m=m*2390;

//initialize the phase matrix so that at n=0 the nuclei are in equilibrium separation and are at rest

for (i=0;i<=N+1;i++){

	//generate random initial phase for i-th nucleus according to Maxwell distribution
	srand48(i*i*traj*RandSeed);
	r=2;
	while (r>1){
	q=2*drand48()-1;
	s=2*drand48()-1;
	r=q*q+s*s;
	}
	r=sqrt((-2*log(r))/r);
	phase[1][i] = ( (i!=0) && (i!=N+1) ) ? (s*r*sqrt(T[2]*kb/m)) : (0);//initial velocity
	phase[0][i] = xeq*i;//initial position
//	phase[0][i]+= ( (i!=0) && (i!=N+1) ) ? (q*r*sqrt(T[2]*kb/m)*dt) : (0);
	printf("%.4f %.5f		",phase[0][i],phase[1][i]);
	F[i]=0;
	FJ[i]=0;
	EJ[i]=0;
	Jacc[i]=0;
	for (j=1;j<=4;j++){
		Q[0][i][j]=0;
		Q[1][i][j]=0;
		V[0][i][j]=0;
		V[1][i][j]=0;
		}
	}




vmelt=sqrt(2*D*(1-exp(-alpha*xeq))*(1-exp(-alpha*xeq))/m);

//=====================================================================================================================================================

printf("\n\ntrj	time	seg.	tot.energy	temp.	0		1		2		3=N		N+1\n");
printf("	[s]		[kcal/mole]	[K]\n\n");



for (n=1;n<=maxstep;n++){

	//generate random forces for current time-step
	srand48(n*n*traj*RandSeed*RandSeed);
	r=2;
	while (r>1){
	q=2*drand48()-1;
	s=2*drand48()-1;
	r=q*q+s*s;
	}
	r=sqrt((-2*log(r))/r);
	R[0]=s*r*sqrt(2*T[0]*kb*m*g[0]/dt);//left bath
	R[1]=q*r*sqrt(2*T[1]*kb*m*g[1]/dt);//right bath


	//print time to phase file
	if (( (n % seglength)==0 ) && (h!=0)){fprintf(PhaseFile,"\n%d",n);}
	else if ((n % seglength)==0 ) fprintf(PhaseFile,"%d",n);

		

	//compute RK4 slopes' tensor V, and accumulate total energy of previous time-step (uses the Morse exponents' vector e, and Morse forces' vector F)

	for (j=1;j<=4;j++){//it is necessary to go over each RK stage separately with each nucleus before advancing to the next stage, because we need e[i] and e[i+1] to get F[i] at each RK4 stage, j
		e[0]=exp(-1.0*alpha*(phase[0][1]+dt*Q[0][1][j]-phase[0][0]-dt*Q[0][0][j]-xeq))-1;//Morse potential exponent between first nucleus and left bath, minus 1 ( e = exp{alpha*delta_x} - 1 )

		Eacc += (j==1) ? (D*e[0]*e[0]+m*phase[1][0]*phase[1][0]/2) : (0);//include adsorption energy in the total energy term. and it should be that phase[1][0]=phase[1][N+1]=0
		for (i=1;i<=N;i++){

			for (k=0;k<=1;k++){//here, k=0 refers to the position, and k=1 to the velocity

				//deffine the RK4 adjustment factors' tensor Q, where k=0 refers to adjustments using v[i][j-1] and k=1 refers to a[i][j-1]
				switch(j){
					case 1 :
						Q[k][i][j]=0;
						break;
					case 2 :
						Q[k][i][j]=V[k][i][1]/2;
						break;
					case 3 :
						Q[k][i][j]=V[k][i][2]/2;
						break;
					case 4 :
						Q[k][i][j]=V[k][i][3];
					}

				}
			//calculate Morse exponentials for each atom (i) in this Runga-Kutta interation (j) and for the current time-step (n)
			e[i]=exp(-1.0*alpha*(phase[0][i+1]+dt*Q[0][i+1][j]-phase[0][i]-dt*Q[0][i][j]-xeq))-1;//e[i] is the Morse exponent between nucleus i and the one to its right (i+1), minus 1 ( e = exp{alpha*delta_x} - 1 )
			Eacc += (j==1) ? (D*e[i]*e[i]+m*phase[1][i]*phase[1][i]/2) : (0);
			}

		for (i=1;i<=N;i++){
			
			F[i]=-2.0*alpha*D*((e[i]+1)*e[i]-(e[i-1]+1)*e[i-1]);//net force on atom [i] in the positive x direction

			F[1]+= (i==1) ? (-1.0*m*g[0]*(phase[1][1]+dt*Q[1][1][j])+R[0]) : (0);//add dissipation and random force to nucleus 1
			F[N]+= (i==N) ? (-1.0*m*g[1]*(phase[1][N]+dt*Q[1][N][j])+R[1]) : (0);//add dissipation and random force to nucleus N

			V[1][i][j]=F[i]/m;			//V[1][i][j] refers to a[i][j] and
			V[0][i][j]=phase[1][i]+dt*Q[1][i][j];	//V[0][i][j] refers to v[i][j]			

			//note that V[1]~Q[0] and V[0]~Q[1] this is because the two orders of the ODE are interelated (dv/dt= -dH/dx -g*v , dx/dt=v)
			//V[1]~dv/dt	V[0]~dx/dt


/* ########################################################################################################################################################################################################################
//			step-by-step printout of as many vital variables as can fit in the width of the screen:
//
//			printf("%d %d %d	%+3.2e	%+3.2e	%+3.3f	%+3.2e %+3.2e		%+3.3f %+3.3f	%+3.3f %+3.3f\n",n,j,i,Eacc,e[i],F[i],V[1][i][j],V[0][i][j],Q[1][i][j],Q[0][i][j],phase[1][2],phase[0][2]);
#########################################################################################################################################################################################################################*/


			if ( (j==1) && (n % seglength)==0 ) fprintf(PhaseFile," %e",phase[0][i]);
				}

			}
		

	//calculate pair-wise forces for heat current calculation

	//for the heat current from each bath
	FJ[0]=-1.0*m*g[0]*(phase[1][1]+dt*Q[1][1][1])+R[0];//dissipation and random force on nucleus 1
	FJ[N]=-1.0*m*g[1]*(phase[1][N]+dt*Q[1][N][1])+R[1];//dissipation and random force on nucleus N
	Jacc[0] += phase[1][1]*FJ[0];
	Jacc[N] += phase[1][N]*FJ[N];

	//for the heat current between nuclei
	for (i=1;i<=N-1;i++){//note that this loop calculates FJ[1] to FJ[N-1] whereas the F[i] loop went from 1 to N

		FJ[i]=-2.0*alpha*D*((e[i]+1)*e[i]);//force between atoms [i] and [i+1] in the positive x direction

		//Q[k][i][1] = 0, so the dissipation and random force of the beginning of the step are taken this should not matter when averaging over times much longer than dt

		Jacc[i] += phase[1][i]*FJ[i];// the accumulated heat current entering nucleus i+1 from its left (except for Jacc[N] which means the heat current leaving nucleus N to the right)

		}

		FJ0=-2.0*alpha*D*((e[0]+1)*e[0]);//force on atom [0] from the left wall in the positive x direction
		FJN=-2.0*alpha*D*((e[N]+1)*e[N]);//force on atom [N] from the right wall in the positive x direction
		Jacc0 += -1.0*phase[1][0]*FJ0;
		JaccN += -1.0*phase[1][N]*FJN;

//.....................................................................................................................................................

	//move the N nuclei using the RK4 slopes' tensor V computed in the previous section, the 0 and N+1 sites are unchanged thorughout

	for (i=1;i<=N;i++){
		for (k=0;k<=1;k++){
			phase[k][i]+=(V[k][i][1]+2*V[k][i][2]+2*V[k][i][3]+V[k][i][4])*dt/6;


/* ###############################################################################
//			step-by-step printout of phases, line-by-line:
//
//			printf("%d	%d	%d	%lf\n",n,i,k,phase[k][i]);
################################################################################*/

			
			}

			if ((phase[0][i]<0) || (phase[0][i]>((N+1)*xeq))){
				printf("\n\n\n\n		MELT\n\n\n\n");
				return(100);
				}
		}

//.....................................................................................................................................................





	if ((n % seglength)==0){

		//outputting energy as a function of time [in femtoseconds]
		fprintf(outfile,"%e %e\n",n*dt,Eacc/(seglength*kb));
		printf("\r%d	%.1e	%d	%.3f		%.3f	",traj,n*dt*pow(10,-15),h+1,Eacc/seglength,Eacc/(seglength*kb));

		fprintf(HeatCurrent,"%e",n*dt);
		fprintf(HeatTransfered,"%e",n*dt);
		fprintf(HeatContinuity,"%e",n*dt);

		for (i=0;i<=N;i++){
			printf("	%.3f %.3f",phase[0][i],phase[1][i]);
			fprintf(HeatCurrent," %e",1000.0*Jacc[i]/(seglength*kb));// average output heat current from the left of nucleus i+1 (exept for Jacc[N] which means the heat current leaving nucleus N to the right) for this segment

//**********************************************************************************************************************

			EJ[i]+=Jacc[i]*dt;//total heat transfered so far from the left of nucleus i+1

//**********************************************************************************************************************


			fprintf(HeatTransfered," %e",EJ[i]/kb);
			fprintf(HeatContinuity," %e",(EJ[i]-1.0*(T[2]*kb-Eacc/seglength))/kb);
			Jacc[i]=0;
			}

//			printf("	%.3f %.3f",phase[0][N+1],phase[1][N+1]);

		EJ0+=Jacc0*dt;
		EJN+=JaccN*dt;
		fprintf(HeatCurrent," %e %e\n",1000.0*Jacc0/(seglength*kb),1000.0*JaccN/(seglength*kb));// average output heat current from the left- and right-most nuclei to their respective wall
		fprintf(HeatTransfered," %e %e\n",EJ0/kb,EJN/kb);
		fprintf(HeatContinuity," %e %e\n",(EJ0-1.0*(T[2]*kb-Eacc/seglength))/kb,(EJN-1.0*(T[2]*kb-Eacc/seglength))/kb);
		Jacc0=0;
		JaccN=0;

//		fprintf(HeatCurrent,"\n");
//		fprintf(HeatTransfered,"\n");
//		fprintf(HeatContinuity,"\n");
		
		Eacc=0;
		h++;
		fflush(stdout);//can be removed so that the update counter jumps (isn't continuous) however the entire program runs more quickly
		}

}


//=====================================================================================================================================================

fprintf(PhaseFile," \n");

printf("\n\n");
printf("Initial ensemble energy	%lf	%lf\n",T[2]*kb,T[2]);
printf("			[kca/mole]	[kB]\n\n");
fclose(HeatCurrent);
fclose(HeatTransfered);
fclose(HeatContinuity);
fclose(PhaseFile);
fclose(outfile);
return 0;
}

//=====================================================================================================================================================

