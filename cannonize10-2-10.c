#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//./cannonize.out $EFile $PhaseFile $JFile $TransfFile $ContFile 
//		  $TempEFile $TempPhaseFile $TempJFile $TempTransfFile $TempContFile 
//		  $E $Phase $J $Trans $Cont $RandomizerSeed

//----------------------------------------------------------------
// get the number of the current trajectory (traj)
// get a randomization seed (RandSeed)
// get 15 file names:
//			5 input file names which are the output of the last run of ./dvira5.out {FileE}
//			5 input file names which are the ensemble averaged so far (all previous trajectories) {TempFileE}
//			5 output file names which are the new ensemble average (include the last trajectory) {E}
//----------------------------------------------------------------



int main(int argc, char *argv[]){

//=================================================================================
//arguments passed to the program from the command line:
//------------------------------------------------------
//
//argc is the number of command line arguments
//argv is an array of the command line arguments as strings
//envp is a pointer to the array of environment variables, not used in this program
//=================================================================================

	if (argc!=19){ printf("\n\nwrong number of command line arguments in 'cannonize.out', aborting\n\n"); return 100; }

	int	i,traj=atoi(argv[1]),N=atoi(argv[18]);
//	double	RandSeed=atoi(argv[2]);

/*	char	*FileE=argv[3],*FilePhase=argv[4],*FileJ=argv[5],*FileTrans=argv[6],*FileCont=argv[7];
	char	*TempFileE=argv[8],*TempFilePhase=argv[9],*TempFileJ=argv[10],*TempFileTrans=argv[11],*TempFileCont=argv[12];
	char	*E=argv[13],*Phase=argv[14],*J=argv[15],*Trans=argv[16],*Cont=argv[17];
*/

	FILE 	*FileE	= fopen(argv[3],"rt");
		if (FileE==NULL) {fputs ("\n\nError opening file FileE\n",stderr); exit (1);}
	FILE 	*FilePhase	= fopen(argv[4],"rt");
		if (FilePhase==NULL) {fputs ("\n\nError opening file FilePhase\n",stderr); exit (1);}
	FILE 	*FileJ	= fopen(argv[5],"rt");
		if (FileJ==NULL) {fputs ("\n\nError opening file FileJ\n",stderr); exit (1);}
	FILE 	*FileTrans	= fopen(argv[6],"rt");
		if (FileTrans==NULL) {fputs ("\n\nError opening file FileTrans\n",stderr); exit (1);}
	FILE 	*FileCont	= fopen(argv[7],"rt");
		if (FileCont==NULL) {fputs ("\n\nError opening file FileCont\n",stderr); exit (1);}

	FILE 	*TempFileE	= fopen(argv[8],"rt");
		if (TempFileE==NULL) {fputs ("\n\nError opening file TempFileE\n",stderr); exit (2);}
	FILE 	*TempFilePhase	= fopen(argv[9],"rt");
		if (TempFilePhase==NULL) {fputs ("\n\nError opening file TempFilePhase\n",stderr); exit (2);}
	FILE 	*TempFileJ	= fopen(argv[10],"rt");
		if (TempFileJ==NULL) {fputs ("\n\nError opening file TempFileJ\n",stderr); exit (2);}
	FILE 	*TempFileTrans	= fopen(argv[11],"rt");
		if (TempFileTrans==NULL) {fputs ("\n\nError opening file TempFileTrans\n",stderr); exit (2);}
	FILE 	*TempFileCont	= fopen(argv[12],"rt");
		if (TempFileCont==NULL) {fputs ("\n\nError opening file TempFileCont\n",stderr); exit (2);}

	FILE 	*E	= fopen(argv[13],"wt");
		if (E==NULL) {fputs ("\n\nError opening file E\n",stderr); exit (3);}
	FILE 	*Phase	= fopen(argv[14],"wt");
		if (Phase==NULL) {fputs ("\n\nError opening file Phase\n",stderr); exit (3);}
	FILE 	*J	= fopen(argv[15],"wt");
		if (J==NULL) {fputs ("\n\nError opening file J\n",stderr); exit (3);}
	FILE 	*Trans	= fopen(argv[16],"wt");
		if (Trans==NULL) {fputs ("\n\nError opening file Trans\n",stderr); exit (3);}
	FILE 	*Cont	= fopen(argv[17],"wt");
		if (Cont==NULL) {fputs ("\n\nError opening file Cont\n",stderr); exit (3);}


	
	double	LastTraj[N+4],PrevTraj[N+4];
	int	mismatch=1;				// line number of mismatching time indeces


//average E
	while ( feof(FileE) == 0 ){	//while the f_1 file hasn't returned end_of_file:

		fscanf(FileE,"%lf %lf\n",&LastTraj[0],&LastTraj[1]);
		fscanf(TempFileE,"%lf %lf\n",&PrevTraj[0],&PrevTraj[1]);

		// if the time indeces agree... ( OR if this is the first trajectory, so there is only one set of data)
		if ((LastTraj[0]==PrevTraj[0]) || (traj==1)){

			fprintf(E,"%lf %lf\n",LastTraj[0],(LastTraj[1]+(traj-1)*PrevTraj[1])/traj);

		}

		// ...otherwise notify and exit
		else{
			printf("\nin line %d of %s time indeces to dot match\n",mismatch,argv[3]);
			return 200;
		}

		mismatch++;//advance mismatch line counter

		}

mismatch=1;
//average Phase
	while ( feof(FilePhase) == 0 ){	//while the f_1 file hasn't returned end_of_file:

		for(i=0;i<=N;i++){

			fscanf(FilePhase,"%lf ",&LastTraj[i]);
			fscanf(TempFilePhase,"%lf ",&PrevTraj[i]);
		}

		fscanf(FilePhase,"\n");
		fscanf(TempFilePhase,"\n");

		// if the time indeces agree... ( OR if this is the first trajectory, so there is only one set of data)
		if ((LastTraj[0]==PrevTraj[0]) || (traj==1)){

		fprintf(Phase,"%lf ",LastTraj[0]);
		for(i=1;i<=N;i++){fprintf(Phase,"%lf ",(LastTraj[i]+(traj-1)*PrevTraj[i])/traj);}
		fprintf(Phase,"\n");

		}

		// ...otherwise notify and exit
		else{
			printf("\nin line %d of %s time indeces to dot match\n",mismatch,argv[4]);
			return 200;
		}

		mismatch++;//advance mismatch line counter

		}

mismatch=1;
//average J
	while ( feof(FileJ) == 0 ){	//while the f_1 file hasn't returned end_of_file:

		for(i=0;i<=N+3;i++){

			fscanf(FileJ,"%lf ",&LastTraj[i]);
			fscanf(TempFileJ,"%lf ",&PrevTraj[i]);
		}

		fscanf(FileJ,"\n");
                fscanf(TempFileJ,"\n");


		// if the time indeces agree... ( OR if this is the first trajectory, so there is only one set of data)
		if ((LastTraj[0]==PrevTraj[0]) || (traj==1)){


		fprintf(J,"%lf ",LastTraj[0]);
                for(i=1;i<=N+3;i++){fprintf(J,"%lf ",(LastTraj[i]+(traj-1)*PrevTraj[i])/traj);}
                fprintf(J,"\n");


		}

		// ...otherwise notify and exit
		else{
			printf("\nin line %d of %s time indeces to dot match\n",mismatch,argv[5]);
			return 200;
		}

		mismatch++;//advance mismatch line counter

		}

mismatch=1;
//average Trans
	while ( feof(FileTrans) == 0 ){	//while the f_1 file hasn't returned end_of_file:


		for(i=0;i<=N+3;i++){

			fscanf(FileTrans,"%lf ",&LastTraj[i]);
			fscanf(TempFileTrans,"%lf ",&PrevTraj[i]);
		}

		fscanf(FileTrans,"\n");
                fscanf(TempFileTrans,"\n");


		// if the time indeces agree... ( OR if this is the first trajectory, so there is only one set of data)
		if ((LastTraj[0]==PrevTraj[0]) || (traj==1)){


		fprintf(Trans,"%lf ",LastTraj[0]);
                for(i=1;i<=N+3;i++){fprintf(Trans,"%lf ",(LastTraj[i]+(traj-1)*PrevTraj[i])/traj);}
                fprintf(Trans,"\n");


		}

		// ...otherwise notify and exit
		else{
			printf("\nin line %d of %s time indeces to dot match\n",mismatch,argv[6]);
			return 200;
		}

		mismatch++;//advance mismatch line counter

		}

mismatch=1;
//average Cont
	while ( feof(FileCont) == 0 ){	//while the f_1 file hasn't returned end_of_file:


		for(i=0;i<=N+3;i++){

			fscanf(FileCont,"%lf ",&LastTraj[i]);
			fscanf(TempFileCont,"%lf ",&PrevTraj[i]);
		}

		fscanf(FileCont,"\n");
                fscanf(TempFileCont,"\n");


		// if the time indeces agree... ( OR if this is the first trajectory, so there is only one set of data)
		if ((LastTraj[0]==PrevTraj[0]) || (traj==1)){


		fprintf(Cont,"%lf ",LastTraj[0]);
                for(i=1;i<=N+3;i++){fprintf(Cont,"%lf ",(LastTraj[i]+(traj-1)*PrevTraj[i])/traj);}
                fprintf(Cont,"\n");

		}

		// ...otherwise notify and exit
		else{
			printf("\nin line %d of %s time indeces to dot match\n",mismatch,argv[7]);
			return 200;
		}

		mismatch++;//advance mismatch line counter

		}


	fclose(E);
	fclose(Phase);
	fclose(J);
	fclose(Trans);
	fclose(Cont);

	fclose(FileE);
	fclose(FilePhase);
	fclose(FileJ);
	fclose(FileTrans);
	fclose(FileCont);

	fclose(TempFileE);
	fclose(TempFilePhase);
	fclose(TempFileJ);
	fclose(TempFileTrans);
	fclose(TempFileCont);


	return 0;
}

