# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

  1. dvira*.out is a 4th-order Runga-Kutta integrator in C, used to propagate a Molecular Dynamics trajectory.
  2. Run dvira*.out "MaxTraj"-many times, each time with eparate initial conditions (randomly generated from a Maxwell-Boltzmann distribution at temperature "Tsys").
  3. Calls the C program 'ensemble.out' to join the output from previous runs ('TempEFile', etc.) with that of the current run ('EFile', etc.), giving each of the two the appropriate weight, and outputs into a third file ('E', etc.)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: [Inon Sharony](mailto:InonShar@TAU.ac.IL)
* Other community or team contact